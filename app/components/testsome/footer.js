import React from 'react'

//这里是太长不想贴上来的css代码..

export default class Footer extends React.Component {
  constructor() {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    this.props.onClick()
  }

  render() {
    return (
      <div>
        <footer>
          <a onClick={this.handleClick}>click footer to back</a>
        </footer>
      </div>
    )
  }

}

Footer.propTypes = {
  onClick: React.PropTypes.func.isRequired
}