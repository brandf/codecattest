import React, {
	Component
} from 'react'

class UserItem extends Component {
	constructor(props) {
		super(props)
		const {
			name
		} = this.props.data
		this.state = {
			data: {
				name: name,
				age: this.props.data.detail.age,
				sex: this.props.data.detail.sex
			}
		}
	}
	handleInputChange(key, e) {
		let tar = this.state.data
		tar[key] = e.target.value
		this.setState({
			data: tar
		})
	}
	handleDeleteUser() {
		const {
			deleteUser
		} = this.props.data
		const {
			toggleUserDetail
		} = this.props
		deleteUser()
		toggleUserDetail()
	}
	handleUserUpdate() {
		const {
			updateUser
		} = this.props
		let newUserData = {
			id: this.props.data.id,
			name: this.state.data.name,
			detail: {
				age: this.state.data.age,
				sex: this.state.data.sex
			}
		}
		console.log(newUserData)
		updateUser(newUserData)
	}
	render() {
		console.log(this.state)
		const {
			toggleUserDetail
		} = this.props
		return (
			<div>
				<div className="dialog-mark"></div>
				<div className="dialog-box" style={{height:'450px'}}>
					<div className="dialog paper">
						<div className="dialog-content c-text-b">
							<div className="ta-c"><img className="img-circle bg-tran w72 h72 va-m mb16" src={require("../../images/bg_test.jpg")}/></div>
							<div className="mt8">
								<div className="input">
						            <label className="fs12 c-grey">姓名:</label>
						            <br/>
						            <input type="text" className="input-down" placeholder="输入姓名" defaultValue={this.state.data.name} maxLength="15" onChange={this.handleInputChange.bind(this,'name')} required/>
						        </div>
							</div>
							<div className="mt8">
								<div className="input">
						            <label className="fs12 c-grey">性别:</label>
						            <br/>
						            <input type="text" className="input-down" placeholder="输入性别" defaultValue={this.state.data.sex} maxLength="15" onChange={this.handleInputChange.bind(this,'sex')} required/>
						        </div>
							</div>
							<div className="mt8">
								<div className="input">
						            <label className="fs12 c-grey">年龄:</label>
						            <br/>
						            <input type="text" className="input-down" placeholder="输入年龄" defaultValue={this.state.data.age} maxLength="15" onChange={this.handleInputChange.bind(this,'age')} required/>
						        </div>
							</div>
						</div>
						<div className="ta-c ptb8">
							<button className="btn btn-s btn-tp c-blue mr8" onClick={this.handleDeleteUser.bind(this)}>删除</button>
							<button className="btn btn-s btn-tp c-blue mr8" onClick={this.handleUserUpdate.bind(this)}>更新</button>
							<button className="btn btn-s btn-tp c-blue" onClick={toggleUserDetail}>关闭</button>
						</div>			
					</div>
					
				</div>
			</div>

		)
	}
}

export default UserItem