import {
	addReducer
} from '../store.js'
var user_list = [{
	id: 0,
	name: '张三',
	img: '../../images/bg_test.jpg',
	detail: {
		'sex': '男',
		'age': '22',
	}
}, {
	id: 1,
	name: '李四',
	img: '../../images/bg_test.jpg',
	detail: {
		'sex': '男',
		'age': '21',
	}
}, {
	id: 2,
	name: '陈五',
	img: '../../images/bg_test.jpg',
	detail: {
		'sex': '女',
		'age': '21',
	}
}]
localStorage.setItem("datalist", JSON.stringify(user_list))
var id = 1


export const fetchUserList = () => {
	return dispatch => {
		dispatch({
			type: 'FETCH_USERLIST_SUCCESS',
			items: JSON.parse(localStorage.getItem("datalist"))
		})
	}
}

export const addUser = (data) => {
	return dispatch => {
		console.log('add')
		console.log(id)
		console.log(data)
		dispatch({
			type: 'ADD_USERDATA_SUCCESS',
			data: Object.assign({}, data, {
				id: ++id
			})
		})
	}
}

export const deleteUser = (user_id) => {
	return dispatch => {
		console.log('del')
		console.log(user_id)
		dispatch({
			type: 'DELETE_USERDATA_SUCCESS',
			id: user_id
		})
	}
}

export const updateUser = (data) => {
	return dispatch => {
		dispatch({
			type: 'UPDATE_USERDATA_SUCCESS',
			userData: data
		})
	}
}

export const toggleUserDetail = (isToggle) => {
	console.log(isToggle)
	return dispatch => {
		dispatch({
			type: 'TOGGLEUSERDETAIL_SUCCESS',
			isToggle: isToggle
		})
	}
}

const userListReducer = (state = {
	isFetching: false,
	items: [{}],
	isToggle: false
}, action) => {
	switch (action.type) {
		case 'FETCH_USERLIST_SUCCESS':
			return Object.assign({}, state, {
				items: action.items
			})
		case 'ADD_USERDATA_SUCCESS':
			return Object.assign({}, state, {
				items: [...state.items, action.data]
			})
		case 'DELETE_USERDATA_SUCCESS':
			var _selfState = state
			for (var i = _selfState.items.length - 1; i >= 0; i--) {
				if (_selfState.items[i].id == action.id) {
					_selfState.items.splice(i, 1)
					break;
				}
			}
			return Object.assign({}, state, {
				items: [..._selfState.items]
			})
		case 'TOGGLEUSERDETAIL_SUCCESS':
			return Object.assign({}, state, {
				isToggle: action.isToggle
			})
		case 'UPDATE_USERDATA_SUCCESS':
			var _selfState = state
			for (var i = _selfState.items.length - 1; i >= 0; i--) {
				if (_selfState.items[i].id == action.userData.id) {
					_selfState.items.splice(i, 1, action.userData)
					break;
				}
			}
			return Object.assign({}, state, {
				items: [..._selfState.items]
			})
		default:
			return state
	}
}

addReducer(userListReducer)