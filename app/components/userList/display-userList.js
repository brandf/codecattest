import React, {
	Component
} from 'react'
import UserItem from './display-userItem.js'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

class UserList extends Component {
	constructor(props) {
		super(props)
		this.state = {
			data: {
				name: "",
				age: "",
				sex: ""
			},
			isToggle: false,
			userDetail: {}
		}
		this.initialUserList = this.props.initialUserList.bind(this)
	}
	componentDidMount() {
		this.initialUserList()
	}
	handleInputChange(key, e) {
		let tar = this.state.data
		tar[key] = e.target.value
		this.setState({
			data: tar
		})
	}
	handleToggleDetail(data) {
		const {
			toggleUserDetail
		} = this.props
		this.setState({
			userDetail: data
		})
		toggleUserDetail(true)
	}

	render() {
		const {
			addUser,
			deleteUser,
			toggleUserDetail,
			updateUser
		} = this.props
		return (
			<div className="ta-c">
				<p className="fs24 pt24"><strong>用户列表</strong></p>
				<ul className="paper menu m8 ta-l">
					{
						Array.prototype.map.call(this.props.lists,(elem, index) =>{
							var data=Object.assign({},elem,{deleteUser:deleteUser.bind(this,elem.id)})

							return(
								<li key={index} className="user-list" onClick={this.handleToggleDetail.bind(this,data)}>
									<a><img src={require("../../images/bg_test.jpg")} className="img-circle bg-tran w72 h72 va-m m8"/>{elem.name}</a>		
								</li>
							)
						})
					}
					{
						this.props.isToggle?(
							<ReactCSSTransitionGroup
								transitionName="example"
								transitionAppear={true}
								transitionAppearTimeout={500}
								transitionEnter={false}
								transitionLeave={false}>
								<UserItem data={this.state.userDetail} toggleUserDetail={toggleUserDetail.bind(this,false)} updateUser={updateUser}/>
							</ReactCSSTransitionGroup>
						):''
					}
				</ul>
				<div className="add-user m32 ta-l">
					<div className="mt16">
						<input onChange={this.handleInputChange.bind(this,'name')} type="text" name="user_name" id="user_name" className="input-s input-normal" maxLength="15" placeholder="输入你的姓名" required/>
					</div>
					<div className="mt16">
						<input onChange={this.handleInputChange.bind(this,'age')} type="text" name="user_age" id="user_age" className="input-s input-normal" maxLength="15" placeholder="输入你的年龄" required/>
					</div>
					<div className="mt16">
						<input onChange={this.handleInputChange.bind(this,'sex')} type="text" name="user_sex" id="user_sex" className="input-s input-normal" maxLength="15" placeholder="输入你的性别" required/>
					</div>
					<div className="mt16">
						<button className="btn btn-m bg-blue c-text-w" onClick={addUser.bind(this,{name:this.state.data.name,img:'img.src',detail:{sex:this.state.data.sex,age:this.state.data.age}})}>新增用户</button>
					</div>
				</div>
			</div>
		)
	}
}

export default UserList