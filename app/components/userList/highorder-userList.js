import {
	connect
} from 'react-redux';
import UserList from './display-userList.js'
import {
	fetchUserList,
	addUser,
	deleteUser,
	toggleUserDetail,
	updateUser
} from './data-userList.js'

const mapStateToProps = (state) => {
	return {
		lists: state.userListReducer.items,
		isToggle: state.userListReducer.isToggle
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
		initialUserList: () => dispatch(fetchUserList()),
		addUser: (data) => dispatch(addUser(data)),
		deleteUser: (id) => dispatch(deleteUser(id)),
		toggleUserDetail: (isToggle) => dispatch(toggleUserDetail(isToggle)),
		updateUser: (data) => dispatch(updateUser(data))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)