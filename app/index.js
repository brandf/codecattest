require('./sass/main/slucky.scss');
require('./css/bug.css');
require('./css/styles.css');

import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import {
	Provider,
	connect
} from 'react-redux'

import {
	getStore,
	updateReducer
} from './components/store.js'
import UserList from './components/userList/highorder-userList.js'

const store = getStore()

ReactDOM.render(
	<Provider store={store}>
     	<UserList/>
  	</Provider>,
	document.getElementById('root')
)